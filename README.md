# HAWK Authorisation Test Client #
Generating a HMAC to access 'The Works' can be cumbersome.
This client will generate a key for the supplied URL and log it out (for you to Copy & Paste), but it will also perform simple requests. If you're just *getting*, this might be enough.

**Please note that the generated key is valid for only 2 minutes**

## How do I use it? ##

node . URL *METHOD*

URL is required, METHOD is a HTTP method and is optional (defaults to GET)