// Seeing as it can be a bit fun to create the authentication header, lets use hawk to do the hard work


var Request = require( 'request' );
var Hawk = require( 'hawk' );

// Client credentials
var credentials = {
    id: 'AKIAIZTFBQF24N655UUA',
    key: 'N+XXVblu/iFsY4GEbuxJ5cllwdRrIYlIv5CtUXm7',
    algorithm: 'sha256'
}
var arguments = process.argv.splice( 2 );
var argUri = arguments[0];
var argMethod = 'GET';

if( !argUri ) {
	console.log( 'No URL argument supplied' );
	process.exit( 0 );
}

if( arguments[1] ) {
	argMethod = arguments[1]
}

// Request options
var requestOptions = {
    uri: argUri,
    method: argMethod,
    headers: {}
};
console.log();
var header = Hawk.client.header(requestOptions.uri, requestOptions.method, { credentials: credentials, ext: 'some-app-data' });
requestOptions.headers.Authorization = header.field;

console.log( 'Copy Auth header from below' );
// If we want to test this in another app (postman?) we can copy the generated header 
console.log();
console.log( requestOptions.headers.Authorization );
console.log();
Request(requestOptions, function (error, response, body) {
	if( response ) {
		// Authenticate the server's response
		var isValid = Hawk.client.authenticate(response, credentials, header.artifacts, { payload: body });
		// Output results
		console.log( 'RESPONSE' );
		console.log(response.statusCode + ': ' + body + (isValid ? ' (valid)' : ' (invalid)'));
	} else {
		console.log( 'No reply from supplied URL' );
		process.exit( 0 );
	}
});
